#!/bin/bash

# The aim of this script is to update docker images on production machine and resart them


#echo "Updating ssl sertificates..."
#sudo cp -r -l /etc/letsencrypt/live/app.studygram.pro/ /home/michael/ssl/

echo "Checking 'docker-compose.yml'..."
docker-compose config
if [ $? -eq 0 ]; then
    echo OK
else
    echo FAIL
    exit 1
fi

echo "Pulling new containers..."
docker-compose pull
if [ $? -eq 0 ]; then
    echo OK
else
    echo FAIL
    exit 1
fi

echo "Restarting docker-compose..."
docker-compose --env-file ./prod_configs/.env.prod up -d
if [ $? -eq 0 ]; then
    echo OK
else
    echo FAIL
    exit 1
fi

echo "Cleaning unused docker stuff..."
docker system prune
if [ $? -eq 0 ]; then
    echo OK
else
    echo FAIL
    exit 1
fi

echo "Done"
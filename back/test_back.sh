#!/bin/bash

echo "Exporting DEV env variables..."
export $(grep -v '^#' $PWD/../.env.dev | xargs)
export POSTGRES_DB=${POSTGRES_DB_TEST}

export FILE_STORE_PATH=$PWD/../db_files_test/

echo "Creaeting test db..."
export PGPASSWORD=${POSTGRES_PASSWORD}
psql -U ${POSTGRES_USER} -h ${POSTGRES_HOST} -p ${POSTGRES_PORT} -c "CREATE DATABASE ${POSTGRES_DB_TEST};"
mkdir $(${FILE_STORE_PATH})

echo "Testing..."
pytest

echo "Droppping test db..."
psql -U ${POSTGRES_USER} -h ${POSTGRES_HOST} -p ${POSTGRES_PORT} -c "DROP DATABASE ${POSTGRES_DB_TEST};"
rm -rf $(${FILE_STORE_PATH})

#!/bin/bash

export $(grep -v '^#' ../../.env.dev | xargs)

docker run -it \
  --rm --name rabbit \
  -p ${RABBIT_PORT}:5672 \
  -p 15672:15672 \
  -e RABBITMQ_DEFAULT_USER=${RABBIT_USER} \
  -e RABBITMQ_DEFAULT_PASS=${RABBIT_PASSWORD} \
  rabbitmq:3.9-management

import json
from time import sleep

import pika
from pika.spec import Basic, BasicProperties
from pika.adapters.blocking_connection import BlockingChannel

from rabbit.utils import get_rabbit_connection

TEST_QUEUE_NAME = "rabbit-test-queue"
RABBIT_TEST_DATA = json.dumps({"rabbit-test-key": "rabbit-test-value"})
channel_was_called = False


def _channel_consume_callback(channel: BlockingChannel, method: Basic.Deliver, properties: BasicProperties, body: bytes) -> None:
    global channel_was_called
    assert method.routing_key == TEST_QUEUE_NAME, f"hm, routing_key mismatch"
    data_str = body.decode()
    assert data_str == RABBIT_TEST_DATA, (
        f"expect:\n`{RABBIT_TEST_DATA}`\ntype: {type(RABBIT_TEST_DATA)}\ngot:\n`{data_str}`\ntype: {type(data_str)}"
    )
    channel_was_called = True

    channel.basic_ack(delivery_tag=method.delivery_tag)

    channel.stop_consuming()


def test_simple():
    global channel_was_called

    # create connection and declare queue
    rabbit_connection = get_rabbit_connection()
    rabbit_channel = rabbit_connection.channel()
    rabbit_channel.queue_delete(queue=TEST_QUEUE_NAME)
    rabbit_channel.queue_declare(queue=TEST_QUEUE_NAME)

    rabbit_channel.basic_consume(queue=TEST_QUEUE_NAME, on_message_callback=_channel_consume_callback, auto_ack=False)

    channel_was_called = False
    rabbit_channel.basic_publish(
        exchange='',
        routing_key=TEST_QUEUE_NAME,
        body=RABBIT_TEST_DATA,
    )
    rabbit_channel.start_consuming()

    sleep(0.1)
    assert channel_was_called, f"No read from channel"

    # check message count in a queue
    res = rabbit_channel.queue_declare(queue=TEST_QUEUE_NAME)
    assert res.method.message_count == 0

    rabbit_channel.close()

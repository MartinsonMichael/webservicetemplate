import logging
import os
from time import sleep

from pika.exceptions import AMQPConnectionError
from typing import Optional

import pika

env = os.environ

# amqp://username:password@host:port/<virtual_host>[?query-string]
_RabbitConnection = pika.connection.URLParameters(
    f"amqp://{env['RABBIT_USER']}:{env['RABBIT_PASSWORD']}@{env['RABBIT_HOST']}:{env['RABBIT_PORT']}/"
)

MATCHER_INPUT_QUEUE = "matcher-input-queue"
LINKEDIN_UPDATER_INPUT_QUEUE = "linkedin-updated-input-queue"


def get_rabbit_connection(
        max_attempts: int = 5,
        logger: Optional[logging.Logger] = None,
) -> Optional[pika.BlockingConnection]:

    connection_attempts = 0
    rabbit_connection = None
    while connection_attempts < max_attempts:
        try:
            rabbit_connection = pika.BlockingConnection(_RabbitConnection)
            break
        except AMQPConnectionError:
            connection_attempts += 1
            if logger is not None:
                logger.warning(f"Rabbit isn't responding. Waiting... [{connection_attempts} / {max_attempts}]")
            sleep(3)

    if rabbit_connection is None:
        if logging is not None:
            logger.critical(f"Rabbit isn't responding. Failed. Exited.")
        raise AMQPConnectionError()

    if logger is not None:
        logger.info("Connection with Rabbit established.")

    return rabbit_connection

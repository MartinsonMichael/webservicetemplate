## <TEMPLATE>. Backend

### Folder structure

__Note:__ all configs here are development, for produciton configs see [`prod_configs`](/prod_configs)

* `db` - database related stuff, currently schemas descriptions are made with SQLAlchemy
* `alembic` - tool for db migrations
* `rabbit` - message query, RabbitMQ, with `pika` - python library for access
* `redis_cache` - in-memory cache by Redis
* `jwt_keys` - development RSA keys for JWT
* `webapi` - folder with web API

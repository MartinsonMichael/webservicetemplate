# This file is generated, DO NOT EDIT IT
# Michael Martinson http generator (c)
#

from typing import List, Dict, Union, Any, Optional, Dict
import json


class RegisterRequest:
    def __init__(self, email: str, login: str, password: str):
        self.email: str = email
        self.login: str = login
        self.password: str = password

    def to_json(self) -> Union[Dict, List]:
        return {
            'email': self.email,
            'login': self.login,
            'password': self.password,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'RegisterRequest':
        return RegisterRequest(
            email=obj['email'],
            login=obj['login'],
            password=obj['password'],
        )


class LoginRequest:
    def __init__(self, email: str, password: str):
        self.email: str = email
        self.password: str = password

    def to_json(self) -> Union[Dict, List]:
        return {
            'email': self.email,
            'password': self.password,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'LoginRequest':
        return LoginRequest(
            email=obj['email'],
            password=obj['password'],
        )


class AuthResponse:
    def __init__(self, user_id: int, login: str, token: str):
        self.user_id: int = user_id
        self.login: str = login
        self.token: str = token

    def to_json(self) -> Union[Dict, List]:
        return {
            'user_id': self.user_id,
            'login': self.login,
            'token': self.token,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'AuthResponse':
        return AuthResponse(
            user_id=obj['user_id'],
            login=obj['login'],
            token=obj['token'],
        )


class Token:
    def __init__(self, token: str):
        self.token: str = token

    def to_json(self) -> Union[Dict, List]:
        return {
            'token': self.token,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'Token':
        return Token(
            token=obj['token'],
        )


class Project:
    def __init__(self, project_id: int, title: str, color: str, section_list: List[str]):
        self.project_id: int = project_id
        self.title: str = title
        self.color: str = color
        self.section_list: List[str] = section_list

    def to_json(self) -> Union[Dict, List]:
        return {
            'project_id': self.project_id,
            'title': self.title,
            'color': self.color,
            'section_list': self.section_list,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'Project':
        return Project(
            project_id=obj['project_id'],
            title=obj['title'],
            color=obj['color'],
            section_list=obj['section_list'],
        )


class ProjectList:
    def __init__(self, list: List['Project']):
        self.list: List['Project'] = list

    def to_json(self) -> Union[Dict, List]:
        return {
            'list': [x.to_json() for x in self.list],
        }

    @staticmethod
    def from_json(obj: Dict) -> 'ProjectList':
        return ProjectList(
            list=[Project.from_json(x) for x in obj['list']],
        )


class NewProject:
    def __init__(self, title: str, color: str):
        self.title: str = title
        self.color: str = color

    def to_json(self) -> Union[Dict, List]:
        return {
            'title': self.title,
            'color': self.color,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'NewProject':
        return NewProject(
            title=obj['title'],
            color=obj['color'],
        )


class Task:
    def __init__(self, task_id: int, title: str, description: str):
        self.task_id: int = task_id
        self.title: str = title
        self.description: str = description

    def to_json(self) -> Union[Dict, List]:
        return {
            'task_id': self.task_id,
            'title': self.title,
            'description': self.description,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'Task':
        return Task(
            task_id=obj['task_id'],
            title=obj['title'],
            description=obj['description'],
        )



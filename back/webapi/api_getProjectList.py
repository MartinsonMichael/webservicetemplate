from typing import Tuple, Union
import webapi.generated_messages as msg


def getProjectList(user_id: int) -> Tuple[Union[msg.ProjectList, str], int]:
    """
    :param user_id: int - id of user who perform this api method
    :return  Tuple of two objects:
         * first - msg.ProjectList | string - msg.ProjectList if function call was successful,
            string with error description otherwise
         * second - int - http code, if code not in range 200-299,
            then the first object expected to be a string with error description
    """

    raise NotImplemented


import logging
import os

import flask
from flask import request, jsonify, Response
from typing import Tuple, Union
from werkzeug.utils import secure_filename

from flask_jwt_extended import (
    create_access_token, get_jwt_identity, jwt_required, JWTManager
)

from redis_cache.utils import redis_client
import webapi.processing as utils

FILE_STORE_PATH = os.environ['FILE_STORE_PATH']
ALLOWED_EXTENSIONS = {'zip'}

app = flask.Flask(__name__)
app.config['UPLOAD_FOLDER'] = FILE_STORE_PATH

app.config["JWT_SECRET_KEY"] = open(os.environ['JWT_PRIVATE_KEY_PATH'], 'rb').read()
jwt = JWTManager(app)

logger: logging.Logger = logging.getLogger(__name__)
logger.setLevel(os.environ.get("LOGGING_LEVEL", "DEBUG"))


@app.post("/api/register")
def login():
    """
    Expect body : {"email": str, "username": str, "password": str}
    :return: {"access_token": str}

    NOTE: frontend must set heder: {'Authorization' "Bearer <value of access_token>"}
        exactly with way, with a space after "Bearer"
    """
    data, code = utils.registration(request.json)
    if code != 200:
        return data, code

    access_token = create_access_token(identity=data['user_id'])
    return jsonify(access_token=access_token)


@app.post("/api/login")
def login():
    """
    Expect body : {"email": str, "password": str}
    :return: {"access_token": str}

    NOTE: frontend must set heder: {'Authorization' "Bearer <value of access_token>"}
        exactly with way, with a space after "Bearer"
    """
    data, code = utils.registration(request.json)
    if code != 200:
        return data, code

    access_token = create_access_token(identity=data['user_id'])
    return jsonify(access_token=access_token)


# --- START OF GENERATED PART: DO NOT EDIT CODE BELLOW --- #


import webapi.generated_messages as msg
from webapi.api_getProjectList import getProjectList
from webapi.api_addProject import addProject
from webapi.api_makeNewTaskFromFile import makeNewTaskFromFile


@app.post('/api/getProjectList')
@jwt_required()
def api_method_getProjectList() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    output_data, code = getProjectList(user_id)
    if 200 <= code <= 299:
        return output_data, code
    assert isinstance(output_data, msg.ProjectList), \
        f"Wrong type of output_data, should be 'ProjectList', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


@app.post('/api/addProject')
@jwt_required()
def api_method_addProject() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    input_data: msg.NewProject = msg.NewProject.from_json(request.json)
    output_data, code = addProject(input_data, user_id)
    if 200 <= code <= 299:
        return output_data, code
    assert isinstance(output_data, msg.Project), \
        f"Wrong type of output_data, should be 'Project', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


@app.post('/api/makeNewTaskFromFile')
@jwt_required()
def api_method_makeNewTaskFromFile() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    output_data, code = makeNewTaskFromFile(user_id, request.files)
    if 200 <= code <= 299:
        return output_data, code
    assert isinstance(output_data, msg.Task), \
        f"Wrong type of output_data, should be 'Task', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


# --- START OF GENERATED PART: DO NOT EDIT CODE UPPER --- #



@app.get("/health")
@app.get("/api/health")
def get_health_status():
    return "Tamplete:WebAPI - OK", 200


@app.errorhandler(404)
def error(err):
    return "wrong url. Try `/health`", 404

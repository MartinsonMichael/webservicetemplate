from typing import Tuple, Union
import webapi.generated_messages as msg


def addProject(input_data: msg.NewProject, user_id: int) -> Tuple[Union[msg.Project, str], int]:
    """
    :param input_data: object of type NewProject (see generated methods for more info)
    :param user_id: int - id of user who perform this api method
    :return  Tuple of two objects:
         * first - msg.Project | string - msg.Project if function call was successful,
            string with error description otherwise
         * second - int - http code, if code not in range 200-299,
            then the first object expected to be a string with error description
    """

    raise NotImplemented


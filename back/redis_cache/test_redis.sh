#!/bin/bash

export $(grep -v '^#' ../../.env.dev | xargs)

pytest --capture=tee-sys

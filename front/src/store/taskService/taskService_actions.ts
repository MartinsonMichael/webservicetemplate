// This file is generated, DO NOT EDIT IT
// Michael Martinson http generator (c)


import { axiosInstance } from "../client"
import * as msg from "../generated_messages"
import { AxiosResponse } from "axios";




export const getProjectList_START = "getProjectList_START";
interface getProjectList_START_Action {
    type: typeof getProjectList_START
    payload: undefined
}
export const getProjectList_SUCCESS = "getProjectList_SUCCESS";
interface getProjectList_SUCCESS_Action {
    type: typeof getProjectList_SUCCESS
    payload: msg.ProjectList
}
export const getProjectList_REJECTED = "getProjectList_REJECTED";
interface getProjectList_REJECTED_Action {
    type: typeof getProjectList_REJECTED
    payload: string
}

export const getProjectList = () => {
    return async (dispatch: any) => {
        dispatch({type: getProjectList_START, payload: undefined});

        await axiosInstance.post(
            'getProjectList',
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.ProjectList, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: getProjectList_SUCCESS, payload: msg.construct_ProjectList(response.data)});
                } else {
                    dispatch({type: getProjectList_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: getProjectList_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const addProject_START = "addProject_START";
interface addProject_START_Action {
    type: typeof addProject_START
    payload: undefined
}
export const addProject_SUCCESS = "addProject_SUCCESS";
interface addProject_SUCCESS_Action {
    type: typeof addProject_SUCCESS
    payload: msg.Project
}
export const addProject_REJECTED = "addProject_REJECTED";
interface addProject_REJECTED_Action {
    type: typeof addProject_REJECTED
    payload: string
}

export const addProject = (title: string, color: string) => {
    return async (dispatch: any) => {
        dispatch({type: addProject_START, payload: undefined});

        await axiosInstance.post(
            'addProject',
            {
                'title': title,
                'color': color,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.Project, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: addProject_SUCCESS, payload: msg.construct_Project(response.data)});
                } else {
                    dispatch({type: addProject_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: addProject_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const makeNewTaskFromFile_START = "makeNewTaskFromFile_START";
interface makeNewTaskFromFile_START_Action {
    type: typeof makeNewTaskFromFile_START
    payload: undefined
}
export const makeNewTaskFromFile_SUCCESS = "makeNewTaskFromFile_SUCCESS";
interface makeNewTaskFromFile_SUCCESS_Action {
    type: typeof makeNewTaskFromFile_SUCCESS
    payload: msg.Task
}
export const makeNewTaskFromFile_REJECTED = "makeNewTaskFromFile_REJECTED";
interface makeNewTaskFromFile_REJECTED_Action {
    type: typeof makeNewTaskFromFile_REJECTED
    payload: string
}

export const makeNewTaskFromFile = (files: FormData) => {
    return async (dispatch: any) => {
        dispatch({type: makeNewTaskFromFile_START, payload: undefined});

        await axiosInstance.post(
            'makeNewTaskFromFile',
            files,
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.Task, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: makeNewTaskFromFile_SUCCESS, payload: msg.construct_Task(response.data)});
                } else {
                    dispatch({type: makeNewTaskFromFile_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: makeNewTaskFromFile_REJECTED, payload: error.toString()});
            }
        );
    }
};



export type TaskServiceActionType = (
    getProjectList_START_Action |
    getProjectList_SUCCESS_Action |
    getProjectList_REJECTED_Action |
    addProject_START_Action |
    addProject_SUCCESS_Action |
    addProject_REJECTED_Action |
    makeNewTaskFromFile_START_Action |
    makeNewTaskFromFile_SUCCESS_Action |
    makeNewTaskFromFile_REJECTED_Action 
)
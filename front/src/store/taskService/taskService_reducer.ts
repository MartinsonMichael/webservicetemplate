import * as msg from "../generated_messages"
import { TaskServiceActionType } from "./taskService_actions"


export interface TaskServiceState {
    // TODO add valuable state, probably rename

    isLoading: boolean
    error?: string
}

const initialState: TaskServiceState = {
    // TODO add valuable state, probably rename

    isLoading: false,
    error: undefined,
} as TaskServiceState;


export function TaskServiceReducer(state = initialState, action: TaskServiceActionType): TaskServiceState {
    switch (action.type) {
        case "getProjectList_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as TaskServiceState;

        case "getProjectList_SUCCESS":
            return {
                ...state,
                isLoading: false,
                error: undefined,
            } as TaskServiceState;

        case "getProjectList_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as TaskServiceState;


        case "addProject_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as TaskServiceState;

        case "addProject_SUCCESS":
            return {
                ...state,
                isLoading: false,
                error: undefined,
            } as TaskServiceState;

        case "addProject_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as TaskServiceState;


        case "makeNewTaskFromFile_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as TaskServiceState;

        case "makeNewTaskFromFile_SUCCESS":
            return {
                ...state,
                isLoading: false,
                error: undefined,
            } as TaskServiceState;

        case "makeNewTaskFromFile_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as TaskServiceState;


        default:
            return state
    }
}

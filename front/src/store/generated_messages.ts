// This file is generated, DO NOT EDIT IT
// Michael Martinson http generator (c)


export interface RegisterRequest {
    email: string
    login: string
    password: string
}
export function construct_RegisterRequest(x: any): RegisterRequest {
    return x as RegisterRequest
}


export interface LoginRequest {
    email: string
    password: string
}
export function construct_LoginRequest(x: any): LoginRequest {
    return x as LoginRequest
}


export interface AuthResponse {
    user_id: number
    login: string
    token: string
}
export function construct_AuthResponse(x: any): AuthResponse {
    return x as AuthResponse
}


export interface Token {
    token: string
}
export function construct_Token(x: any): Token {
    return x as Token
}


export interface Project {
    project_id: number
    title: string
    color: string
    section_list: string[]
}
export function construct_Project(x: any): Project {
    return x as Project
}


export interface ProjectList {
    list: Project[]
}
export function construct_ProjectList(x: any): ProjectList {
    return {
        list: x['list'].map((item: any) => construct_Project(item)),
    } as ProjectList
}


export interface NewProject {
    title: string
    color: string
}
export function construct_NewProject(x: any): NewProject {
    return x as NewProject
}


export interface Task {
    task_id: number
    title: string
    description: string
}
export function construct_Task(x: any): Task {
    return x as Task
}



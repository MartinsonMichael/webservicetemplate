import {combineReducers} from "redux";
import {AuthServiceReducer} from "./authService/authService_reducer";


export const rootReducer = combineReducers({
    auth: AuthServiceReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
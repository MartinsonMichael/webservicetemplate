// This file is generated, DO NOT EDIT IT
// Michael Martinson http generator (c)


import { axiosInstance } from "../client"
import * as msg from "../generated_messages"
import { AxiosResponse } from "axios";




export const register_START = "register_START";
interface register_START_Action {
    type: typeof register_START
    payload: undefined
}
export const register_SUCCESS = "register_SUCCESS";
interface register_SUCCESS_Action {
    type: typeof register_SUCCESS
    payload: msg.AuthResponse
}
export const register_REJECTED = "register_REJECTED";
interface register_REJECTED_Action {
    type: typeof register_REJECTED
    payload: string
}

export const register = (email: string, login: string, password: string) => {
    return async (dispatch: any) => {
        dispatch({type: register_START, payload: undefined});

        await axiosInstance.post(
            'register',
            {
                'email': email,
                'login': login,
                'password': password,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.AuthResponse, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: register_SUCCESS, payload: msg.construct_AuthResponse(response.data)});
                } else {
                    dispatch({type: register_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: register_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const login_START = "login_START";
interface login_START_Action {
    type: typeof login_START
    payload: undefined
}
export const login_SUCCESS = "login_SUCCESS";
interface login_SUCCESS_Action {
    type: typeof login_SUCCESS
    payload: msg.AuthResponse
}
export const login_REJECTED = "login_REJECTED";
interface login_REJECTED_Action {
    type: typeof login_REJECTED
    payload: string
}

export const login = (email: string, password: string) => {
    return async (dispatch: any) => {
        dispatch({type: login_START, payload: undefined});

        await axiosInstance.post(
            'login',
            {
                'email': email,
                'password': password,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.AuthResponse, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: login_SUCCESS, payload: msg.construct_AuthResponse(response.data)});
                } else {
                    dispatch({type: login_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: login_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const updateToken_START = "updateToken_START";
interface updateToken_START_Action {
    type: typeof updateToken_START
    payload: undefined
}
export const updateToken_SUCCESS = "updateToken_SUCCESS";
interface updateToken_SUCCESS_Action {
    type: typeof updateToken_SUCCESS
    payload: msg.Token
}
export const updateToken_REJECTED = "updateToken_REJECTED";
interface updateToken_REJECTED_Action {
    type: typeof updateToken_REJECTED
    payload: string
}

export const updateToken = () => {
    return async (dispatch: any) => {
        dispatch({type: updateToken_START, payload: undefined});

        await axiosInstance.post(
            'updateToken',
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.Token, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: updateToken_SUCCESS, payload: msg.construct_Token(response.data)});
                } else {
                    dispatch({type: updateToken_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: updateToken_REJECTED, payload: error.toString()});
            }
        );
    }
};



export type AuthServiceActionType = (
    register_START_Action |
    register_SUCCESS_Action |
    register_REJECTED_Action |
    login_START_Action |
    login_SUCCESS_Action |
    login_REJECTED_Action |
    updateToken_START_Action |
    updateToken_SUCCESS_Action |
    updateToken_REJECTED_Action 
)
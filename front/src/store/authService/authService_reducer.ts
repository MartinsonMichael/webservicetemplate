import * as msg from "../generated_messages";
import { AuthServiceActionType } from "./authService_actions";
import { axiosInstance } from "../client";
import Cookies from "universal-cookie/es6";

export interface AuthServiceState {
    isAuth: boolean;
    email: string;
    userName: string;

    isLoading: boolean;
    error?: string;
}

const initialState: AuthServiceState = {
    isAuth: localStorage.getItem("token") !== null,
    email: "",
    userName: "",

    isLoading: false,
    error: undefined,
} as AuthServiceState;

export function setToken(token: string) {
    localStorage.setItem("token", token);
    axiosInstance.defaults.headers.common["token"] = token;
    const cookies = new Cookies();
    cookies.set("token", token, { path: "/" });
}

export function AuthServiceReducer(
    state = initialState,
    action: AuthServiceActionType
): AuthServiceState {
    switch (action.type) {
        case "register_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as AuthServiceState;

        case "register_SUCCESS":
            setToken(action.payload.token);
            return {
                ...state,
                isAuth: true,
                userName: action.payload.login,
                isLoading: false,
                error: undefined,
            } as AuthServiceState;

        case "register_REJECTED":
            return {
                ...state,
                isAuth: false,
                isLoading: false,
                error: action.payload,
            } as AuthServiceState;

        case "login_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as AuthServiceState;

        case "login_SUCCESS":
            setToken(action.payload.token);
            return {
                ...state,
                isAuth: true,
                userName: action.payload.login,
                isLoading: false,
                error: undefined,
            } as AuthServiceState;

        case "login_REJECTED":
            return {
                ...state,
                isAuth: false,
                isLoading: false,
                error: action.payload,
            } as AuthServiceState;

        case "updateToken_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as AuthServiceState;

        case "updateToken_SUCCESS":
            setToken(action.payload.token);
            return {
                ...state,
                isAuth: true,
                isLoading: false,
                error: undefined,
            } as AuthServiceState;

        case "updateToken_REJECTED":
            return {
                ...state,
                isAuth: false,
                isLoading: false,
                error: action.payload,
            } as AuthServiceState;

        default:
            return state;
    }
}

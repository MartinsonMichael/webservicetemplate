// This file is generated, DO NOT EDIT IT
// Michael Martinson http generator (c)


import axios from "axios";
import Cookies from "universal-cookie";

const cookies = new Cookies();
cookies.set("token", localStorage.getItem("token"), { path: "/" });


export const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_API_BACKEND_ADDRESS + "/api/",
    responseType: "json",
    headers: {
        token: localStorage.getItem("token") || "",
    },
});

import React from "react";
import { useParams } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";

import { RootState } from "../store";
import * as msg from "../store/generated_messages";

const mapStoreStateToProps = (store: RootState) => ({
    isLoading: store.auth.isLoading,
    error: store.auth.error,
});
const mapDispatchToProps = (dispatch: any) => {
    return {
    };
};
const connector = connect(mapStoreStateToProps, mapDispatchToProps);

type HomePageProps = ConnectedProps<typeof connector> & {};


const Home: React.FC<HomePageProps> = (props: HomePageProps): React.ReactElement => {
    return (
        <div>
            Hello!
        </div>
    );
};

export default connector(Home);

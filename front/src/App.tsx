import React from 'react';
import {BrowserRouter, Navigate, useRoutes} from 'react-router-dom';

import Home from "./pages/Home";


// see https://typescript.tv/react/upgrade-to-react-router-v6/
const App: React.FC = (): JSX.Element => {
    const mainRoutes = {
        path: '/',
        element: <Home />,
    };

  const routing = useRoutes([mainRoutes]);

  return <>{routing}</>;
};

export default App;

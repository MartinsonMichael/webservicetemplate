# <TEMPLATE> project

## Development structure

Folders:
* [`back`](/back/README.md) - folder with backend related stuff
* [`front`](/front/README.md) - folder with frontend related stuff
* [`prod_configs`](/prod_configs/README.md) - super secret configs with production settings (passwords, access keys, etc.)

Files:
* `.gitlab-ci.yml` - description of ci/cd for gitlab
* `gitlab_deploy.sh` - technical script for deployment